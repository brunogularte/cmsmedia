<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\galeria;

use App\galeriafoto;

use App\Foto;

class galleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = galeria::all('*');
        return view('galeria.index', ['galerias' => $results]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $results = Foto::where('statusFoto', '!=', '1')->get();
        return view('galeria.create', ['fotos' => $results]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $galeria = new Galeria;


        $galeria->nomeGaleria = request('nomeGaleria');
        $galeria->tipoGaleria = request('tipoGaleria');

        $galeria->save();

        $fotos = request('multi');

        foreach ($fotos as $foto) {
           $galeriaFoto = new galeriaFoto;

           $galeriaFoto->idFoto = $foto;
           $galeriaFoto->idGaleria = $galeria->id;

           $galeriaFoto->save();

           Foto::where('idFoto', '=', $foto)->update(['statusFoto' => 1]);
        }

        return redirect("gallery")->with('message', 'A Galeria foi Adicionada com Sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $galeria = galeria::where('idGaleria', '=' , $id)->get();
        $fotosGaleria = galeriaFoto::where('idGaleria', '=', $id)->get();

        $todasAsFotos = [];
        foreach ($fotosGaleria as $fotoGaleria) {
            $foto = Foto::where('idFoto', '=', $fotoGaleria->idFoto)->get();
            array_push($todasAsFotos, $foto);
        }

        $data =[

            'galeria' => $galeria,
            'fotos' => $todasAsFotos

        ];

        return view('galeria.show', ['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $galeria = galeria::where('idGaleria', '=' , $id);
        $galeria->delete();

        return redirect("gallery")->with('message', 'A Galeria foi Deletada com Sucesso!');
    }
}
