<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\galeria;

use App\galeriafoto;

use App\Foto;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $galerias = galeria::all('*');
       $todasAsFotos = [];

       foreach ($galerias as $galeria) {
           $fotos = galeriaFoto::where('idGaleria', '=', $galeria->idGaleria)->get();

           $fotosComAtt = [];
           foreach ($fotos as $foto) {
               $umaFoto = Foto::where("idFoto", '=', $foto->idFoto)->get();
               array_push($fotosComAtt, $umaFoto);
           }


           array_push($todasAsFotos, $fotosComAtt);
       }
       return view('home', ['galerias' => $galerias, 'fotos' => $todasAsFotos]);
    }
}
