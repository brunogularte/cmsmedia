<?php

namespace App\Http\Controllers;

use App\Agenda;

use Illuminate\Http\Request;

use App\Http\Requests;

class agendaController extends Controller
{
    public function index()
    {
        $results = Agenda::all('*');
        return view('agenda.index', ['eventos' => $results]);
    }
  
    public function create()
    {
        return view('agenda.create');
    }
 
    public function store()
    {

        $agenda = new Agenda;

        $agenda->nomeEvento = request('nomeEvento');
        $agenda->localEvento = request('localEvento');
        $agenda->horaEvento = request('horaEvento');
        $agenda->dataEvento = request('dataEvento');

        $agenda->save();

        return redirect("agenda")->with('message', 'O Evento foi Adicionado a Agenda com Sucesso!');
    }
  
    public function show($id)
    {
        //
    }
  
    public function edit($id)
    {
        $agenda = Agenda::query()->where('id', $id)->first();
        return view('agenda.edit')->with('agenda', $agenda);
    
}  
    public function update($id)
    {

    /*

        $agenda = Agenda::query()->where('idEvento', $id)->first();*/

        $agenda = Agenda::find($id);

        $agenda->nomeEvento = request('nomeEvento');
        $agenda->localEvento = request('localEvento');
        $agenda->horaEvento = request('horaEvento');
        $agenda->dataEvento = request('dataEvento');

        $agenda->save();

        return redirect()->route('agenda.index')->with('message', 'Evento atualizado com Sucesso!');
    }
  
    public function destroy($id)
    {
        $agenda = Agenda::where('id', '=' , $id);
        $agenda->delete();

        return redirect("agenda")->with('message', 'O evento foi Deletada com Sucesso!');
    }
}
