<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');





Route::resource('photo', 'photoController');

Route::get('photo/edit/{id}', 'photoController@edit');

Route::get('photo/update/{id}', 'photoController@update');

Route::get('photo/create', 'photoController@create');

Route::post('photo/store', 'photoController@store');




Route::resource('gallery', 'galleryController');

Route::get('gallery/edit/{id}', 'galleryController@edit');

Route::get('gallery/delete/{id}', 'galleryController@destroy');

Route::get('gallery/update/{id}', 'galleryController@update');

Route::get('gallery/show/{id}', 'galleryController@show');

Route::get('gallery/create', 'galleryController@create');

Route::post('gallery/store', 'galleryController@store');




Route::resource('about', 'aboutController');




Route::resource('agenda', 'agendaController');

Route::get('agenda/edit/{id}', 'agendaController@edit');

Route::get('agenda/update/{id}', 'agendaController@update');

Route::get('agenda/delete/{id}', 'agendaController@destroy');

Route::post('agenda/store', 'agendaController@store');
