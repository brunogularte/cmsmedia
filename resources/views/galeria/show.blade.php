@extends('layouts.app')

@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            Visualização de Galeria<br>
            <div class="panel panel-default">
                <div class="panel-heading">
                     @foreach($data['galeria'] as $galeria)
                    <h3>{{$galeria->nomeGaleria}}</h3>
                    @endforeach
                </div>

                <div class="panel-body">
                      @foreach($data['fotos'] as $foto)
                        @foreach($foto as $foto1)
                            <div class="card" style="display: inline-grid; width: 18rem;">
                              <img src="/imagens/{{$foto1['arquivoFoto']}}" style=" width: 18rem; border-radius: 2px" class="card-img-top">
                              <div class="card-body">
                                <h5 class="card-title">{{$foto1['descricaoFoto']}}</h5>
                              </div>
                            </div>
                        @endforeach
                      @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
