@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                        <h3>Inserir novo evento para a agenda</h3>
                </div>

                <div class="panel-body">
                    <form method="POST" action="/gallery/store">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label for="nomeEvento">Nome da Galeria</label>
                            <input type="text" class="form-control" name="nomeGaleria" placeholder="Nome do Evento">
                        </div>  

                        <div class="form-group">
                            <label for="localEvento">Tipo Galeria</label>
                            <input type="text" class="form-control" name="tipoGaleria" placeholder="Local do Evento"> 
                        </div>

                                    @foreach($fotos as $foto)


                                    <div class="form-check">
                                      <input class="form-check-input" type="checkbox" name="multi[]" id="multi" value="{{$foto->idFoto}}">
                                      <label class="form-check-label" for="defaultCheck1">
                                        <img src="../imagens/{{$foto->arquivoFoto }}" height="100">
                                      </label>
                                    </div>

                                    @endforeach
                                </select>
                            </div>


                        <button class="btn btn-primary" type="submit">Enviar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
    