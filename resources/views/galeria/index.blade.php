@extends('layouts.app')

@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
         @if (Auth::check())
            <a href="/gallery/create"><button type="button" class="btn btn-success">Criar Galeria</button></a>
            <br><br>
          @else
          @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>Galerias</h3>
                </div>

                <div class="panel-body">


                    @foreach($galerias as $galeria)

                        {{ $galeria->tipoGaleria}}<br>
                        {{ $galeria->nomeGaleria}}<br>
                        <hr>

                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
