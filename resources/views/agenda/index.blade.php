@extends('layouts.app')

@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
             @if (Auth::check())
            <a href="/agenda/create"><button type="button" class="btn btn-success">Criar Evento</button></a>
            <br><br>
          @else
          @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                        <h3>Eventos da Agenda</h3>
                </div>

                <div class="panel-body">


                    @foreach($eventos as $evento)

                        {{ $evento->nomeEvento }}<br>
                        {{ $evento->localEvento }}<br>
                        {{ $evento->horaEvento }}<br>
                        {{ $evento->dataEvento }}<br><br>
                        @if (Auth::check())

                            <a href="/agenda/delete/{{$evento->id}}"><button type="button" class="btn btn-danger">Deletar Evento</button></a>
                            
                          @else
                          @endif

                          <hr>

                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
