@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                        <h3>Inserir novo evento para a agenda</h3>
                </div>

                <div class="panel-body">
                    <form method="POST" action="/agenda/store">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label for="nomeEvento">Nome do Evento</label>
                            <input type="text" class="form-control" name="nomeEvento" placeholder="Nome do Evento">
                        </div>  

                        <div class="form-group">
                            <label for="localEvento">Local do Evento</label>
                            <input type="text" class="form-control" name="localEvento" placeholder="Local do Evento"> 
                        </div>

                        <div class="form-group">
                            <label for="horaEvento">Hora do Evento</label>
                            <input type="text" class="form-control" name="horaEvento" placeholder="Hora do Evento">
                        </div>

                        <div class="form-group">
                            <label for="dataEvento">Data do Evento</label>
                           <input type="text" class="form-control" name="dataEvento" placeholder="Data do Evento">
                         </div>
                         
                        <button class="btn btn-primary" type="submit">Enviar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
