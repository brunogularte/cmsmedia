@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default" style="border: 0px;">
                <div class="panel-heading">Galerias de Fotos</div>

                <div class="panel-body">
                    @foreach ($galerias as $galeria)

                    <div class="panel panel-default">
                        <div class="panel-heading" style="background-color: #5CB85C;">{{$galeria->nomeGaleria}}</div>

                        <div class="row">

                        <div class="panel-body">    

                            <div class='col-sm-2'>
                                <img src="/imagens/galeria.png" style="max-width: 100px;">
                            </div>
                             <div class='col-sm-10'>

                                Tipo: {{$galeria->tipoGaleria}}<br><br>

                            <a href="/gallery/show/{{$galeria->idGaleria}}"><button type="button" class="btn btn-success">Visualizar Galeria</button></a>


                            @if (Auth::check())

                            <a href="/gallery/delete/{{$galeria->idGaleria}}"><button type="button" class="btn btn-danger">Deletar Galeria</button></a>
                            
                          @else
                          @endif
                            </div>

                        </div>

                            
                        </div>
                    </div>

                    @endforeach
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
