@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                        <h3>Inserir nova foto</h3>
                </div>
                <div class="panel-body">
                    <form method="POST" action="/photo/store" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label for="arquivoFoto">Foto: </label>
                            <input type="file" class="form-control" name="arquivoFoto" >
                        </div>  

                        <div class="form-group">
                            <label for="descricaoFoto">Descrição da Foto:</label>
                            <input type="text" class="form-control" name="descricaoFoto" placeholder="Descreva a foto"> 
                        </div>
                        <button class="btn btn-primary" type="submit">Enviar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection