@extends('layouts.app')

@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
             @if (Auth::check())
            <a href="/photo/create"><button type="button" class="btn btn-success">Inserir Foto</button></a>
            <br><br>
          @else
          @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                        <h3>Fotos</h3>
                </div>

                <div class="panel-body">


                    @foreach($fotos as $foto)

                        {{ $foto->descricaoFoto }}<br>
                        <img src="imagens/{{$foto->arquivoFoto }}" height="100">

                        <br>
                        <hr>

                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
