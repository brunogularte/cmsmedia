<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GaleriaFoto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galeriaFoto', function (Blueprint $table) {
            $table->increments('idGaleriaFoto');
            $table->string('idGaleria')
            ->references('idGaleria')->on('galeria')->onDelete('cascade');
            $table->string('idFoto')
            ->references('idFoto')->on('foto')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::table('foto', function (Blueprint $table) {
            $table->string('statusFoto')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
