<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Noticia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create('noticia', function (Blueprint $table) {
            $table->increments('idNoticia');
            $table->string('tituloNoticia');
            $table->string('descricaoNoticia');
            $table->string('linkNoticia');
            $table->timestamps();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
